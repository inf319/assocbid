INF319 - Projeto e Implementação Orientados a Objetos

Aluno: Guilherme Kayo Shida

Comentário:

- Existe uma relacionamento bidirecional entre as classes Pessoa e Companhia;
- A classe Pessoa sabe onde qual a empresa que está relacionada através do atributo emprego;
- A classe Companhia sabe quais são as pessoas da relação através do atributo empregados;
- Ambas as classes podem acessar os métodos públicos da outra através dessa relação.