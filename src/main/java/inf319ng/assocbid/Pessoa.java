package inf319ng.assocbid;

public class Pessoa {
	private String nome;
	private String sobrenome;
	private double salario;
	private Companhia emprego;
	
	public Pessoa() {
		nome = "";
		sobrenome = "";
		salario = 0.0;
	}
	
	public Pessoa(String nome, String sobrenome) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.salario = 0.0;
	}

	public String getNome() {
		return nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public double getSalario() {
		return salario;
	}

	public Companhia getEmprego() {
		return emprego;
	}
	
	public void emprega(Companhia companhia, double salario) {
		this.emprego = companhia;
		this.salario = salario;
	}
	
	public void demite() {
		this.emprego = null;
		this.salario = 0.0;
	}
}
