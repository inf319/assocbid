package inf319ng.assocbid;

import java.util.ArrayList;
import java.util.List;

public class Companhia {
	private String nome_companhia;
	private List<Pessoa> empregados;

	public Companhia() {
		nome_companhia = "";
		empregados = new ArrayList<Pessoa>();
	}

	public Companhia(String nome_companhia) {
		this.nome_companhia = nome_companhia;
		this.empregados = new ArrayList<Pessoa>();
	}

	public String getNome() {
		return nome_companhia;
	}

	public void emprega(Pessoa pessoa, double salario) {
		this.empregados.add(pessoa);
		pessoa.emprega(this, salario);
	}

	public void demite(Pessoa pessoa) {
		this.empregados.remove(pessoa);
		pessoa.demite();
	}

	public double custoTotal() {
		double custoTotal = 0.0;

		for (Pessoa pessoa : empregados) {
			custoTotal += pessoa.getSalario();
		}

		return custoTotal;
	}
}
